import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.rules.JRip;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.RandomForest;
import weka.core.Debug.Random;
import weka.core.Instances;
import weka.core.stopwords.WordsFromFile;
import weka.core.tokenizers.AlphabeticTokenizer;
import weka.core.tokenizers.CharacterNGramTokenizer;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class Start {
	
	/**Idea for this code and some code lines are taken from 
	 * https://www.programcreek.com/java-api-examples/index.php?api=weka.filters.unsupervised.attribute.StringToWordVector
	 * The purpose was to get the classification results for 
	 * as short a time as possible and with as little clicks as possible.
	 * */
	
	public StringToWordVector theBestFilters(Instances input) throws Exception {
		StringToWordVector filter = new StringToWordVector();
		filter.setInputFormat(input);
		filter.setStopwordsHandler(new WordsFromFile());
		filter.setOutputWordCounts(true);
		filter.setLowerCaseTokens(true);
		filter.setDoNotOperateOnPerClassBasis(true);
		filter.setTFTransform(false);
		filter.setIDFTransform(true);
		filter.setTokenizer(new AlphabeticTokenizer());
		return filter;
	}

	public StringToWordVector firstCaseFilter(Instances input, int wordsToKeep) throws Exception {
		System.out.println("First case (" + wordsToKeep + ")----------------------------- ");
		StringToWordVector filter = new StringToWordVector(wordsToKeep);
		filter.setInputFormat(input);
		filter.setStopwordsHandler(new WordsFromFile());
		filter.setOutputWordCounts(true);
		filter.setLowerCaseTokens(true);
		filter.setDoNotOperateOnPerClassBasis(true);
		return filter;
	}

	public StringToWordVector second(Instances input, String tft, String idf) throws Exception {
		System.out.println("Second case-----------------------");
		StringToWordVector filter = new StringToWordVector();
		filter.setInputFormat(input);
		filter.setStopwordsHandler(new WordsFromFile());
		filter.setOutputWordCounts(true);
		filter.setLowerCaseTokens(true);
		filter.setDoNotOperateOnPerClassBasis(true);
		switch (tft) {
		case "true":
			System.out.print("tft = true, ");
			filter.setTFTransform(true);
			if (idf.equals("false")) {
				System.out.println("idf = false ");
				filter.setIDFTransform(false);
			} else {
				System.out.println("idf = true ");
				filter.setIDFTransform(true);
			}
			break;
		case "false":
			System.out.println("tft = false, idf = true ");
			filter.setTFTransform(false);
			filter.setIDFTransform(true);
			break;
		}
		return filter;
	}

	public StringToWordVector third(Instances input, String tokenizer) throws Exception {
		System.out.println("Third case----------------------");
		StringToWordVector filter = new StringToWordVector();
		filter.setInputFormat(input);
		filter.setStopwordsHandler(new WordsFromFile());
		filter.setOutputWordCounts(true);
		filter.setLowerCaseTokens(true);
		filter.setDoNotOperateOnPerClassBasis(true);
		switch (tokenizer) {
		case "Ngram":
			System.out.println("Ngram: ");
			filter.setTokenizer(new NGramTokenizer());
			break;
		case "CharacterNgram":
			System.out.println("CharacterNgram: ");
			filter.setTokenizer(new CharacterNGramTokenizer());
			break;
		case "Alphabetic":
			System.out.println("Alphabetic: ");
			filter.setTokenizer(new AlphabeticTokenizer());
			break;
		}
		return filter;
	}

	private Instances loadData(String fileName, String filterCase) {
		Instances instances = null;
		try {

			BufferedReader br = new BufferedReader(new FileReader(fileName));
			Instances dataset = new Instances(br);
			dataset.setClassIndex(0);

			StringToWordVector filter = new StringToWordVector();
			switch (filterCase) {
			case "First100":
				filter = this.firstCaseFilter(dataset, 100);
				break;
			case "First1000":
				filter = this.firstCaseFilter(dataset, 1000);
				break;
			case "First80": 
				filter = this.firstCaseFilter(dataset, 80);
				break;
			case "SecondTrueFalse":
				filter = this.second(dataset, "true", "false");
				break;
			case "SecondFalseTrue":
				filter = this.second(dataset, "false", "true");
				break;
			case "SecondTrueTrue":
				filter = this.second(dataset, "true", "true");
				break;
			case "ThirdNgram":
				filter = this.third(dataset, "Ngram");
				break;
			case "ThirdCharacterNgram":
				filter = this.third(dataset, "CharacterNgram");
				break;
			case "ThirdAlphabetic":
				filter = this.third(dataset, "Alphabetic");
				break;
			case "final":
				filter = this.theBestFilters(dataset);
				break;
			}

			// filter -- String to word vector
			

			instances = Filter.useFilter(dataset, filter);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}

	
	public static void main(String[] args) {

		Start classify = new Start();

		List<String> fileNames = new ArrayList<>();
		fileNames.add("AGupta.arff");
		fileNames.add("AKumar.arff");
		fileNames.add("CChen.arff");
		fileNames.add("DJohnson.arff");
		fileNames.add("JLee.arff");
		fileNames.add("JMartin.arff");
		fileNames.add("JRobinson.arff");
		fileNames.add("JSmith.arff");
		fileNames.add("KTanaka.arff");
		fileNames.add("MBrown.arff");
		fileNames.add("MJones.arff");
		fileNames.add("MMiller.arff");
		fileNames.add("SLee.arff");
		fileNames.add("YChen.arff");

		for (String file : fileNames) {
			System.out.println("Working with " + file + "---------------");

			List<String> stwvSettings = new ArrayList<>();
			stwvSettings.add("First100");
			stwvSettings.add("First1000");
			stwvSettings.add("First80");
			stwvSettings.add("SecondTrueFalse");
			stwvSettings.add("SecondFalseTrue");
			stwvSettings.add("SecondTrueTrue");
			stwvSettings.add("ThirdNgram");
			stwvSettings.add("ThirdCharacterNgram");
			stwvSettings.add("ThirdAlphabetic");
			stwvSettings.add("final");

			for (String settings : stwvSettings) {
				Instances filtered = classify.loadData(file, settings);
				System.out.println("SIZE = " + filtered.numAttributes());

				List<Classifier> classifiers = new ArrayList<>();
				classifiers.add(new NaiveBayes());
//				classifiers.add(new Logistic());
//				classifiers.add(new MultilayerPerceptron());
				classifiers.add(new IBk());
//				classifiers.add(new JRip());
				classifiers.add(new J48());
				classifiers.add(new RandomForest());
//				classifiers.add(new LMT());
				
				Evaluation eval;
				try {
					eval = new Evaluation(filtered);

					for (Classifier c : classifiers) {
						System.out.print(c.getClass().toString() + ": ");
						eval.crossValidateModel(c, filtered, 10, new Random(1));
						System.out.println("Estimated Accuracy: " + Double.toString(eval.pctCorrect()));

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		System.out.println("--------------end-----------------");

	}

}
